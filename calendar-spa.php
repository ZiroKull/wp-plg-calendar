<?php
/*
Plugin Name: SPA Calendar рабочий
Description: Расписание
Version: 1.1.1
Author: ZiroKull/ktscript
*/


date_default_timezone_set ('Europe/Moscow');
/**
 * Регистрация страницы настроек
 */
add_action('admin_menu', 'options_page_create');

function isAjax() {
	return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
}

function options_page_create() {
    $page_title = 'РАСПИСАНИЕ';
    $menu_title = 'РАСПИСАНИЕ';
    $capability = 'edit_posts';
    $menu_slug = 'spacalendar';
    $function = 'options_page_display';
    $icon_url = '';
    $position = 98;

    add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );

	$page_title_smolnii = 'СМОЛЬНЫЙ';
    $menu_title_smolnii = 'СМОЛЬНЫЙ';
    $capability_smolnii = 'edit_posts';
    $menu_slug_smolnii = 'spacalendarsmolinii';
    $function_smolnii = 'options_page_smolnii_display';
    $icon_url_smolnii = '';
    $position_smolnii = 99;

    add_menu_page( $page_title_smolnii, $menu_title_smolnii, $capability_smolnii, $menu_slug_smolnii, $function_smolnii, $icon_url_smolnii, $position_smolnii );
    
    $vk_title  = 'VK Настройки';
    $vkm_title = 'VK Настройки';
    $vk_slug   = 'vksettings';
    $vk_function = 'vk_page_display';
    
    add_submenu_page( $menu_slug, $vk_title, $vkm_title, 'manage_options', $vk_slug, $vk_function);
}

function getDayRus($day){
	$spa_mon  = get_option('spa_mon',  '');
    $days     = array(
        'Вск', 'Пн', 'Вт', 'Ср',
        'Чт', 'Пт', 'Сб'
    );
    return $days[date('w', strtotime("+". $day ." days -".$spa_mon." hours" ))];
}

function comp($a, $b) 
{
	$result = strcmp($a->post_title, $b->post_title);
	return $result <= 0 ? (!$result ? 0 : -1) : 1;
}

/**
 * Cтраница настроек
 */
function options_page_display() {
	if (!current_user_can('manage_options')) {
        wp_die('Unauthorized user');
    }
	
	$saved = false;

	ksort($_POST['cal_d']);
	ksort($_POST['cal_n']);
	
	if (isset($_POST['cal_d'])) {		
        update_option('cal_d', wp_unslash($_POST['cal_d']), true);
		$saved = true;
    } 
	
	if (isset($_POST['cal_n'])) {		
        update_option('cal_n', wp_unslash($_POST['cal_n']), true);
		$saved = true;
    } 
    
    if (isset($_POST['spa_zone'])) {		
        update_option('spa_zone', $_POST['spa_zone'], true);
		$saved = true;
    } 
    
    if (isset($_POST['spa_mon'])) {		
        update_option('spa_mon', wp_unslash($_POST['spa_mon']), true);
		$saved = true;
    } 
    
    if (isset($_POST['spa_night'])) {		
        update_option('spa_night', wp_unslash($_POST['spa_night']), true);
		$saved = true;
    } 
	
	$cal_d  = get_option('cal_d', '');
	$cal_n  = get_option('cal_n', '');

	
	$spa_zone   = get_option('spa_zone', '+3');
	$spa_mon    = get_option('spa_mon',  '9');
	$spa_night  = get_option('spa_night', '21');
	
	require 'pages/page.php';
}

function options_page_smolnii_display() {
	if (!current_user_can('manage_options')) {
        wp_die('Unauthorized user');
    }
	
	$saved = false;

	ksort($_POST['cal_smolnii_d']);
	ksort($_POST['cal_smolnii_n']);
	
	if (isset($_POST['cal_smolnii_d'])) {		
        update_option('cal_smolnii_d', wp_unslash($_POST['cal_smolnii_d']), true);
		$saved = true;
    }
	
	if (isset($_POST['cal_smolnii_n'])) {		
        update_option('cal_smolnii_n', wp_unslash($_POST['cal_smolnii_n']), true);
		$saved = true;
    }
    
    if (isset($_POST['spa_smolnii_zone'])) {		
        update_option('spa_smolnii_zone', $_POST['spa_smolnii_zone'], true);
		$saved = true;
    } 
    
    if (isset($_POST['spa_smolnii_mon'])) {		
        update_option('spa_smolnii_mon', wp_unslash($_POST['spa_smolnii_mon']), true);
		$saved = true;
    } 
    
    if (isset($_POST['spa_smolnii_night'])) {		
        update_option('spa_smolnii_night', wp_unslash($_POST['spa_smolnii_night']), true);
		$saved = true;
    } 
	
	$cal_smolnii_d  = get_option('cal_smolnii_d', '');
	$cal_smolnii_n  = get_option('cal_smolnii_n', '');

	
	$spa_smolnii_zone   = get_option('spa_smolnii_zone', '+3');
	$spa_smolnii_mon    = get_option('spa_smolnii_mon',  '9');
	$spa_smolnii_night  = get_option('spa_smolnii_night', '21');
	
	require 'pages/page_smolnii.php';
}

/**
 * Cтраница настроек VK
 */
function vk_page_display() {
	if (!current_user_can('manage_options')) {
        wp_die('Unauthorized user');
    }
    
    $saved = false;
	
	if (isset($_POST['vk_settings'])) {
		update_option('vk_desc_d', '');
		update_option('vk_desc_n', '');
		update_option('vk_cont_desc', '');
		update_option('vk_phone', '');
		update_option('vk_adress', '');
		update_option('vk_link', '');
		update_option('vk_tags', '');
		update_option('vk_smile', '');
		update_option('vk_token', '');
		update_option('vk_group', '');	
		
		update_option('vk_zone', '');
		update_option('vk_mon', '');
		update_option('vk_night', '');
	}
	
	if (isset($_POST['vk_publish'])) {
	    do_vk_post();
	}
	
	if (isset($_POST['vk_desc_d'])) {		
        update_option('vk_desc_d', $_POST['vk_desc_d']);
		$saved = true;
    } 
    
    if (isset($_POST['vk_desc_n'])) {		
        update_option('vk_desc_n', $_POST['vk_desc_n']);
		$saved = true;
    } 
    
    if (isset($_POST['vk_cont_desc'])) {		
        update_option('vk_cont_desc', $_POST['vk_cont_desc']);
		$saved = true;
    } 
    
    if (isset($_POST['vk_phone'])) {		
        update_option('vk_phone', $_POST['vk_phone']);
		$saved = true;
    }
    
    if (isset($_POST['vk_adress'])) {		
        update_option('vk_adress', $_POST['vk_adress']);
		$saved = true;
    }
    
    if (isset($_POST['vk_link'])) {		
        update_option('vk_link', $_POST['vk_link']);
		$saved = true;
    }
    
    if (isset($_POST['vk_tags'])) {		
        update_option('vk_tags', $_POST['vk_tags']);
		$saved = true;
    }
    
    if (isset($_POST['vk_smile'])) {		
        update_option('vk_smile', $_POST['vk_smile']);
		$saved = true;
    }
    
    if (isset($_POST['vk_token'])) {		
        update_option('vk_token', $_POST['vk_token']);
		$saved = true;
    }
	
	if (isset($_POST['vk_group'])) {		
        update_option('vk_group', $_POST['vk_group']);
		$saved = true;
    }
    
    if (isset($_POST['vk_zone'])) {		
        update_option('vk_zone', $_POST['vk_zone']);
		$saved = true;
    } 
    
    if (isset($_POST['vk_mon'])) {		
        update_option('vk_mon', $_POST['vk_mon']);
		$saved = true;
    } 
    
    if (isset($_POST['vk_night'])) {		
        update_option('vk_night', $_POST['vk_night']);
		$saved = true;
    } 
	
    $vk_desc_d  = get_option('vk_desc_d', '');
	$vk_desc_n  = get_option('vk_desc_n', '');
	$vk_cont_desc  = get_option('vk_cont_desc', '');
	$vk_phone   = get_option('vk_phone', '');
	$vk_adress  = get_option('vk_adress', '');
	$vk_link    = get_option('vk_link', '');
	$vk_tags    = get_option('vk_tags', '');
	$vk_smile   = get_option('vk_smile', '');
	$vk_token   = get_option('vk_token', '');
	$vk_group   = get_option('vk_group', '');
		
	$vk_zone    = get_option('vk_zone', '+3');
	$vk_mon     = get_option('vk_mon',  '10');
	$vk_night   = get_option('vk_night', '22');
	
    
    require 'pages/vk.php';
}

function load_custom_wp_admin_style($hook) {
	

	if (is_admin()) {
    		if($hook == 'toplevel_page_vksettings') {
        		wp_enqueue_style( 'custom_plugin_css', plugins_url('css/main.css', __FILE__) );
			return;
		}


		if($hook != 'toplevel_page_spacalendar' && $hook != 'toplevel_page_spacalendarsmolinii') {
			return;
		}
	}

	wp_enqueue_style( 'custom_plugin_css', plugins_url('css/main.css', __FILE__) );

	wp_enqueue_script("jquery");
	//wp_enqueue_script( 'custom_jquery', plugins_url('js/jquery.min.js', __FILE__) );   

	wp_enqueue_script( 'custom_bootstrap', plugins_url('js/bootstrap.min.js', __FILE__), array('jquery'));
	wp_enqueue_script( 'custom_plugin_js', plugins_url('js/main.js', __FILE__), array('custom_bootstrap'));
	
	
}
if (is_admin()) 
	add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );
else 
	add_action( 'wp_enqueue_scripts', 'load_custom_wp_admin_style' );



// create a scheduled event (if it does not exist already)
function cronstarter_activation() {
	if (!wp_next_scheduled('vk_post_cron')) {
    	wp_schedule_event( time(), 'hourly', 'vk_post_cron' );
    }
}
// and make sure it's called whenever WordPress loads
add_action('wp', 'cronstarter_activation');

add_action ( 'vk_post_cron',  'my_task_function' );

function my_task_function() {
    
    $vkhours =  gmdate("H", time() + 3600 * get_option('vk_zone', '+3')); 
    $vk_mon    = get_option('vk_mon',  '9');
    $vk_night  = get_option('vk_night',  '22');
    
    if ($vkhours === $vk_mon || $vkhours === $vk_night) {
        do_vk_post();
    }
}

// shortcode masters
add_action('init', function(){
    
    require_once 'vk/vk.php';
	
	add_shortcode( 'calendar-raspisaniy', 'rmcc_calendar_raspisaniy_shortcode' );	
	function rmcc_calendar_raspisaniy_shortcode( $atts ) {
		ob_start();
		
		$cal_d 		= get_option('cal_d', '');
		$cal_n 		= get_option('cal_n', '');
		$spa_mon    = get_option('spa_mon',  '');
		$spa_night  = get_option('spa_night', '');

		?>

			<div class="div-block-724">
				<div class="table w-clearfix">
					<div class="left-part-table">
						<div class="pole-table title-table-copy">Мастера</div>
						
						<?php
							$args = array(
								'post_type' => 'master',
								'post_status'    => 'publish',
								'ignore_sticky_posts' => true,
								'suppress_filters' => true,
								'tax_query' => array( 
									array(
										'taxonomy' => 'master-cat',
										'field'    => 'slug',
										'terms'    => array('rabotayut')
									)
								),
								
								'posts_per_page' => '-1',
								'order'	=> 'DESC',
								'orderby'        => 'title'

							);

							$query = new WP_Query( $args );
							usort($query->posts, "comp");

							if ( $query->have_posts() ) : 
								while ( $query->have_posts() ) : 
									$query->the_post();	
						?>
						
							<div class="pole-table">
								<a href="<?php the_permalink(); ?>" class="div-block-666 w-inline-block">
									<img src="<?php $img = wp_get_attachment_image_src(get_post_thumbnail_id(), "small"); echo $img[0]; ?>" alt="" class="image-33">
								</a>
								<a href="<?php the_permalink(); ?>" class="text-block-19"><?php the_title(); ?></a>
							</div>
									
						<?php
						
								endwhile;  
							endif;   
						?>
						
						
						</div>
					<div class="right-part-table w-clearfix">
						<div class="column-right-table w-clearfix">
							<div class="pole-table title-table right-collumn _58547">
								<div class="text-block-18"><?php echo getDayRus("0"); ?> <?php echo date('d.m', strtotime("+0 days -".$spa_mon." hours")); ?></div>
							</div>
							<div class="pole-table title-table right-collumn _58547">
								<div class="text-block-18"><?php echo getDayRus("1"); ?> <?php echo date('d.m', strtotime("+1 days -".$spa_mon." hours")); ?></div>
							</div>
							<div class="pole-table title-table right-collumn _58547">
								<div class="text-block-18"><?php echo getDayRus("2"); ?> <?php echo date('d.m', strtotime("+2 days -".$spa_mon." hours")); ?></div>
							</div>
							<div class="pole-table title-table right-collumn _58547">
								<div class="text-block-18"><?php echo getDayRus("3"); ?> <?php echo date('d.m', strtotime("+3 days -".$spa_mon." hours")); ?></div>
							</div>
							<div class="pole-table title-table right-collumn _58547">
								<div class="text-block-18"><?php echo getDayRus("4"); ?> <?php echo date('d.m', strtotime("+4 days -".$spa_mon." hours")); ?></div>
							</div>
							<div class="pole-table title-table right-collumn _58547">
								<div class="text-block-18"><?php echo getDayRus("5"); ?> <?php echo date('d.m', strtotime("+5 days -".$spa_mon." hours")); ?></div>
							</div>
							<div class="pole-table title-table right-collumn _58547">
								<div class="text-block-18"><?php echo getDayRus("6"); ?> <?php echo date('d.m', strtotime("+6 days -".$spa_mon." hours")); ?></div>
							</div>
							<div class="pole-table title-table right-collumn _58547">
								<div class="text-block-18"><?php echo getDayRus("7"); ?> <?php echo date('d.m', strtotime("+7 days -".$spa_mon." hours")); ?></div>
							</div>
							<div class="pole-table title-table right-collumn _58547">
								<div class="text-block-18"><?php echo getDayRus("8"); ?> <?php echo date('d.m', strtotime("+8 days -".$spa_mon." hours")); ?></div>
							</div>
							<div class="pole-table title-table right-collumn _58547">
								<div class="text-block-18"><?php echo getDayRus("9"); ?> <?php echo date('d.m', strtotime("+9 days -".$spa_mon." hours")); ?></div>
							</div>
						</div>
						<div class="column-right-table w-clearfix">
							<div class="pole-table right-column-copy">
								<div class="text-block-19">День</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">Ночь</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">День</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">Ночь</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">День</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">Ночь</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">День</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">Ночь</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">День</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">Ночь</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">День</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">Ночь</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">День</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">Ночь</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">День</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">Ночь</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">День</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">Ночь</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">День</div>
							</div>
							<div class="pole-table right-column-copy">
								<div class="text-block-19">Ночь</div>
							</div>
						</div>
						
						<?php

							if ( $query->have_posts() ) : 
								while ( $query->have_posts() ) : 
									$query->the_post();	
						?>
						
						<div class="column-right-table w-clearfix">
							<?php 
								$current = explode('-', date('d-m-H', strtotime("-".$spa_mon." hours")));
							?>
							<?php 
								for ($i = 0; $i<20; $i++) { 

									$checked = false;
									$index_date = date('d.m', strtotime("+". intval($i / 2) ." days -".$spa_mon." hours"));

									if ($i % 2 == 0) {
										if (isset ($cal_d[$index_date][get_the_ID()]))
											$checked = $cal_d[$index_date][get_the_ID()] == 'on' ? 'smena' : '';
											
										if (strlen($checked) &&
											$current[0] == explode('.', $index_date)[0] && 
											intval($current[2]) >= 0 && 
											intval($current[2]) < $spa_night - $spa_mon ) {
											$checked = "now";
										} elseif (strlen($checked) && $current[0] == explode('.', $index_date)[0]) {
											$checked = "late";
										}
									} else {
										if (isset ($cal_n[$index_date][get_the_ID()]))
											$checked = $cal_n[$index_date][get_the_ID()] == 'on' ? 'smena' : '';
										
										if (strlen($checked) && 
											$current[0] == explode('.', $index_date)[0] && 
											intval($current[2]) >= ($spa_night - $spa_mon) && 
											intval($current[2]) < 24 ) {
												$checked = "now";
										}
										
									}	
									
							?>
							
							<div class="pole-table right-column <?= $checked; ?>"></div>
							
							<?php } ?>
							
						</div>
						
						<?php
								endwhile;  
								wp_reset_query(); 
							endif;   
						?>						
						
					</div>
				</div>
			</div>

		<?php
		
		$myvariable = ob_get_clean();
		return $myvariable;
	}
		
		
	
	add_shortcode( 'calendar-onmaster', 'rmcc_calendar_on_master_shortcode' );	
	function rmcc_calendar_on_master_shortcode( $atts ) {
		ob_start();
		
		global $post;
		$post_id = $post->ID;
		$term_list = wp_get_post_terms( $post->ID, 'master-cat', array( 'fields' => 'slugs' ) );

		$cal_d = get_option('cal_d', '');
		$cal_n = get_option('cal_n', '');

		if (in_array("smolnyj", $term_list)) {
			$cal_d = get_option('cal_smolnii_d', '');
			$cal_n = get_option('cal_smolnii_n', '');
		}
		
		$dchecked = '';
		$nchecked = '';
		$dchecked = $cal_d[date('d.m', strtotime("+0 days"))][$post_id] == 'on' ? 'in-salone' : '';
		$nchecked = $cal_n[date('d.m', strtotime("+0 days"))][$post_id] == 'on' ? 'in-salone' : '';
		
		echo 	'<div class="div-block-726">';
		echo 		'<div class="text-block-319">'. getDayRus("0"). ' ' .date('d.m', strtotime("+0 days")) .'</div>';
		echo 		'<div class="div-block-727 srt">';
		echo 			'<div class="div-block-728 '. $dchecked .'"></div>';
		echo 			'<div class="div-block-728 '. $nchecked .'"></div>';
		echo 		'</div>';
		echo 	'</div>';
		
		$dchecked = '';
		$nchecked = '';
		$dchecked = $cal_d[date('d.m', strtotime("+1 days"))][$post_id] == 'on' ? 'in-salone' : '';
		$nchecked = $cal_n[date('d.m', strtotime("+1 days"))][$post_id] == 'on' ? 'in-salone' : '';
		
		echo 	'<div class="div-block-726">';
		echo 		'<div class="text-block-319">'. getDayRus("1"). ' ' .date('d.m', strtotime("+1 days")) .'</div>';
		echo 		'<div class="div-block-727 srt">';
		echo 			'<div class="div-block-728 '. $dchecked .'"></div>';
		echo 			'<div class="div-block-728 '. $nchecked .'"></div>';
		echo 		'</div>';
		echo 	'</div>';
		
		$dchecked = '';
		$nchecked = '';
		$dchecked = $cal_d[date('d.m', strtotime("+2 days"))][$post_id] == 'on' ? 'in-salone' : '';
		$nchecked = $cal_n[date('d.m', strtotime("+2 days"))][$post_id] == 'on' ? 'in-salone' : '';
		
		echo 	'<div class="div-block-726">';
		echo 		'<div class="text-block-319">'. getDayRus("2"). ' ' .date('d.m', strtotime("+2 days")) .'</div>';
		echo 		'<div class="div-block-727 srt">';
		echo 			'<div class="div-block-728 '. $dchecked .'"></div>';
		echo 			'<div class="div-block-728 '. $nchecked .'"></div>';
		echo 		'</div>';
		echo 	'</div>';
		
		$dchecked = '';
		$nchecked = '';
		$dchecked = $cal_d[date('d.m', strtotime("+3 days"))][$post_id] == 'on' ? 'in-salone' : '';
		$nchecked = $cal_n[date('d.m', strtotime("+3 days"))][$post_id] == 'on' ? 'in-salone' : '';
		
		echo 	'<div class="div-block-726">';
		echo 		'<div class="text-block-319">'. getDayRus("3"). ' ' .date('d.m', strtotime("+3 days")) .'</div>';
		echo 		'<div class="div-block-727 srt">';
		echo 			'<div class="div-block-728 '. $dchecked .'"></div>';
		echo 			'<div class="div-block-728 '. $nchecked .'"></div>';
		echo 		'</div>';
		echo 	'</div>';
		
		$dchecked = '';
		$nchecked = '';
		$dchecked = $cal_d[date('d.m', strtotime("+4 days"))][$post_id] == 'on' ? 'in-salone' : '';
		$nchecked = $cal_n[date('d.m', strtotime("+4 days"))][$post_id] == 'on' ? 'in-salone' : '';
		
		echo 	'<div class="div-block-726">';
		echo 		'<div class="text-block-319">'. getDayRus("4"). ' ' .date('d.m', strtotime("+4 days")) .'</div>';
		echo 		'<div class="div-block-727 srt">';
		echo 			'<div class="div-block-728 '. $dchecked .'"></div>';
		echo 			'<div class="div-block-728 '. $nchecked .'"></div>';
		echo 		'</div>';
		echo 	'</div>';
		
		$myvariable = ob_get_clean();
		return $myvariable;
	}
	
	add_shortcode( 'calendar-workdays', 'rmcc_calendar_work_days_shortcode' );	
	function rmcc_calendar_work_days_shortcode( $atts ) {
		ob_start();
		
		global $post;
		$post_id = $post->ID;
		
		$term_list = wp_get_post_terms( $post->ID, 'master-cat', array( 'fields' => 'slugs' ) );

		$cal_d = get_option('cal_d', '');
		$cal_n = get_option('cal_n', '');

		if (in_array("smolnyj", $term_list)) {
			$cal_d = get_option('cal_smolnii_d', '');
			$cal_n = get_option('cal_smolnii_n', '');
		}
		
		$dchecked = '';
		$nchecked = '';
		$dchecked = $cal_d[date('d.m', strtotime("+0 days"))][$post_id] == 'on' ? 'in-salone' : '';
		$nchecked = $cal_n[date('d.m', strtotime("+0 days"))][$post_id] == 'on' ? 'in-salone' : '';
		
		echo 	'<div class="div-block-726">';
		echo 		'<div class="text-block-319">'. getDayRus("0"). ' ' .date('d.m', strtotime("+0 days")) .'</div>';
		echo 		'<div class="div-block-727">';
		echo 			'<div class="div-block-728 '. $dchecked .'"></div>';
		echo 			'<div class="div-block-728 '. $nchecked .'"></div>';
		echo 		'</div>';
		echo 	'</div>';
		
		$dchecked = '';
		$nchecked = '';
		$dchecked = $cal_d[date('d.m', strtotime("+1 days"))][$post_id] == 'on' ? 'in-salone' : '';
		$nchecked = $cal_n[date('d.m', strtotime("+1 days"))][$post_id] == 'on' ? 'in-salone' : '';
		
		echo 	'<div class="div-block-726">';
		echo 		'<div class="text-block-319">'. getDayRus("1"). ' ' .date('d.m', strtotime("+1 days")) .'</div>';
		echo 		'<div class="div-block-727">';
		echo 			'<div class="div-block-728 '. $dchecked .'"></div>';
		echo 			'<div class="div-block-728 '. $nchecked .'"></div>';
		echo 		'</div>';
		echo 	'</div>';
		
		$dchecked = '';
		$nchecked = '';
		$dchecked = $cal_d[date('d.m', strtotime("+2 days"))][$post_id] == 'on' ? 'in-salone' : '';
		$nchecked = $cal_n[date('d.m', strtotime("+2 days"))][$post_id] == 'on' ? 'in-salone' : '';
		
		echo 	'<div class="div-block-726">';
		echo 		'<div class="text-block-319">'. getDayRus("2"). ' ' .date('d.m', strtotime("+2 days")) .'</div>';
		echo 		'<div class="div-block-727">';
		echo 			'<div class="div-block-728 '. $dchecked .'"></div>';
		echo 			'<div class="div-block-728 '. $nchecked .'"></div>';
		echo 		'</div>';
		echo 	'</div>';
		
		$dchecked = '';
		$nchecked = '';
		$dchecked = $cal_d[date('d.m', strtotime("+3 days"))][$post_id] == 'on' ? 'in-salone' : '';
		$nchecked = $cal_n[date('d.m', strtotime("+3 days"))][$post_id] == 'on' ? 'in-salone' : '';
		
		echo 	'<div class="div-block-726">';
		echo 		'<div class="text-block-319">'. getDayRus("3"). ' ' .date('d.m', strtotime("+3 days")) .'</div>';
		echo 		'<div class="div-block-727">';
		echo 			'<div class="div-block-728 '. $dchecked .'"></div>';
		echo 			'<div class="div-block-728 '. $nchecked .'"></div>';
		echo 		'</div>';
		echo 	'</div>';
		
		$dchecked = '';
		$nchecked = '';
		$dchecked = $cal_d[date('d.m', strtotime("+4 days"))][$post_id] == 'on' ? 'in-salone' : '';
		$nchecked = $cal_n[date('d.m', strtotime("+4 days"))][$post_id] == 'on' ? 'in-salone' : '';
		
		echo 	'<div class="div-block-726">';
		echo 		'<div class="text-block-319">'. getDayRus("4"). ' ' .date('d.m', strtotime("+4 days")) .'</div>';
		echo 		'<div class="div-block-727">';
		echo 			'<div class="div-block-728 '. $dchecked .'"></div>';
		echo 			'<div class="div-block-728 '. $nchecked .'"></div>';
		echo 		'</div>';
		echo 	'</div>';
		
		$myvariable = ob_get_clean();
		return $myvariable;
	}
	
	add_shortcode( 'calendar-insalon', 'rmcc_calendar_in_salon_shortcode' );	
	function rmcc_calendar_in_salon_shortcode( $atts ) {
		ob_start();
		
		global $post;
		$post_id = $post->ID;
		
		$hours =  gmdate("H", time() + 3600 * get_option('spa_zone', '+3')); // getdate();	

		$spa_mon    = get_option('spa_mon',  '');
    	$spa_night  = get_option('spa_night', '');
		$cal_d = get_option('cal_d', '');
		$cal_n = get_option('cal_n', '');
		$now     = 'd';
		$out     = 'no';
		$masters = [];
		
		if (($hours > $spa_mon) && ($hours < $spa_night)) {
			$now     = 'd';
		} else {			
			$now     = 'n';
		}
		
		if ($now == 'd') {
			if ($cal_d[date('d.m', strtotime("+0 days "))][$post_id] &&
				$cal_d[date('d.m', strtotime("+0 days"))][$post_id] == 'on') {
				echo 'Сейчас в салоне';
				$out     = 'yes';
			} else if ($cal_d[date('d.m', strtotime("+1 days"))][$post_id] &&
				$cal_d[date('d.m', strtotime("+1 days"))][$post_id] == 'on') {
				echo 'Будет завтра с 9 до 21 часа';
				$out     = 'yes';
			} else if ($cal_n[date('d.m', strtotime("+1 days"))][$post_id] &&
				$cal_n[date('d.m', strtotime("+1 days"))][$post_id] == 'on') {
				echo 'Будет завтра с 21 до 9 часов';
				$out     = 'yes';
			}  else if ($cal_n[date('d.m', strtotime("+0 days"))][$post_id] &&
				$cal_n[date('d.m', strtotime("+0 days"))][$post_id] == 'on') {
				echo 'Сегодня c 21 в салоне';
				$out     = 'yes';
			}
		} else {
			if ($cal_n[date('d.m', strtotime("+0 days"))][$post_id] &&
				$cal_n[date('d.m', strtotime("+0 days"))][$post_id] == 'on') {
				echo 'Сейчас в салоне';
				$out     = 'yes';
			} else if ($cal_d[date('d.m', strtotime("+1 days"))][$post_id] &&
				$cal_d[date('d.m', strtotime("+1 days"))][$post_id] == 'on') {
				echo 'Будет завтра с 9 до 21 часа';
				$out     = 'yes';
			} else if ($cal_n[date('d.m', strtotime("+1 days"))][$post_id] &&
				$cal_n[date('d.m', strtotime("+1 days"))][$post_id] == 'on') {
				echo 'Будет завтра с 21 до 9 часов';
				$out     = 'yes';
			}
		}
		
		$cday = date('d.m', strtotime("+0 days"));
		
		if ($out     == 'no') {
			$dtn = '99.99';
			$dtd = '99.99';
			foreach ($cal_d as $key => $value) {
				if ($key > $cday) {
					if ($cal_d[$key][$post_id] && $cal_d[$key][$post_id] == 'on') {
						$dtd = $key;
						$out = 'yes';
					}
				}
			}
			
			foreach ($cal_n as $key => $value) {
				if ($key > $cday) {
					if ($cal_n[$key][$post_id] && $cal_n[$key][$post_id] == 'on') {
						$dtn = $key;
						$out = 'yes';
					}
				}
			}
			
			$arr = [
				'Января',
				'Февраля',
				'Марта',
				'Апреля',
				'Мая',
				'Июня',
				'Июля',
				'Августа',
				'Сентября',
				'Октября',
				'Ноября',
				'Декабря'
			];
			
			if (($dtd <= $dtn) && ( $dtd != '99.99' ) && ( $dtn != '99.99' )) {
				$time      = strtotime($dtd . '.'. date("Y"));
				$newformat = date('j',$time);
				$month     = date('n',$time) - 1;				
				
				echo 'Будет '. $newformat . ' ' . $arr[$month] .' с 9 часов';
			} else if (($dtd > $dtn)  && ( $dtd != '99.99' ) && ( $dtn != '99.99' )) {
				$time = strtotime($dtn . '.' . date("Y"));
				$newformat = date('j',$time);
				$month     = date('n',$time) - 1;
				
				echo 'Будет '. $newformat . ' ' . $arr[$month] .' с 9 часов';
			}
		}
		
		if ($out     == 'no') {
			echo 'Нет расписания';
		}
		
		$myvariable = ob_get_clean();
		return $myvariable;
	}
		
		
	add_shortcode( 'calendar-work', 'rmcc_calendar_work_parameters_shortcode' );
	function rmcc_calendar_work_parameters_shortcode( $atts ) {
		ob_start();
		
		$hours =  gmdate("H", time() + 3600 * get_option('spa_zone', '+3')); // getdate();	
		$spa_mon    = get_option('spa_mon',  '9');
    	$spa_night  = get_option('spa_night', '21');	
		$cal_d = get_option('cal_d', '');
		$cal_n = get_option('cal_n', '');
		$now     = 'd';
		$out     = 'no';
		$masters = [];
		
		if (($hours > $spa_mon) && ($hours < $spa_night)) {
			$now     = 'd';
		} else {			
			$now     = 'n';
		}
		
		if ($now == 'd') {
			if ($cal_d[date('d.m', strtotime("+0 days"))][get_the_ID()] &&
				$cal_d[date('d.m', strtotime("+0 days"))][get_the_ID()] == 'on') {
				echo '<div class="v-salone">Сейчас в салоне</div>';
				$out     = 'yes';
			} else if ($cal_d[date('d.m', strtotime("+1 days"))][get_the_ID()] &&
				$cal_d[date('d.m', strtotime("+1 days"))][get_the_ID()] == 'on') {
				echo '<div class="not-v-salone">Будет завтра с 9 до 21 часа</div>';
				$out     = 'yes';
			} else if ($cal_n[date('d.m', strtotime("+1 days"))][get_the_ID()] &&
				$cal_n[date('d.m', strtotime("+1 days"))][get_the_ID()] == 'on') {
				echo '<div class="not-v-salone">Будет завтра с 21 до 9 часов</div>';
				$out     = 'yes';
			}  else if ($cal_n[date('d.m', strtotime("+0 days"))][get_the_ID()] &&
				$cal_n[date('d.m', strtotime("+0 days"))][get_the_ID()] == 'on') {
				echo '<div class="not-v-salone">Сегодня c 21 в салоне</div>';
				$out     = 'yes';
			}
		} else {
			if ($cal_n[date('d.m', strtotime("+0 days"))][get_the_ID()] &&
				$cal_n[date('d.m', strtotime("+0 days"))][get_the_ID()] == 'on') {
				echo '<div class="v-salone">Сейчас в салоне</div>';
				$out     = 'yes';
			} else if ($cal_d[date('d.m', strtotime("+1 days"))][get_the_ID()] &&
				$cal_d[date('d.m', strtotime("+1 days"))][get_the_ID()] == 'on') {
				echo '<div class="not-v-salone">Будет завтра с 9 до 21 часа</div>';
				$out     = 'yes';
			} else if ($cal_n[date('d.m', strtotime("+1 days"))][get_the_ID()] &&
				$cal_n[date('d.m', strtotime("+1 days"))][get_the_ID()] == 'on') {
				echo '<div class="not-v-salone">Будет завтра с 21 до 9 часов</div>';
				$out     = 'yes';
			}
		}
		
		$cday = date('d.m', strtotime("+0 days"));
		
		if ($out     == 'no') {
			$dtn = '99.99';
			$dtd = '99.99';
			foreach ($cal_d as $key => $value) {
				if ($key > $cday) {
					if ($cal_d[$key][get_the_ID()] && $cal_d[$key][get_the_ID()] == 'on') {
						$dtd = $key;
						$out = 'yes';
					}
				}
			}
			
			foreach ($cal_n as $key => $value) {
				if ($key > $cday) {
					if ($cal_n[$key][get_the_ID()] && $cal_n[$key][get_the_ID()] == 'on') {
						$dtn = $key;
						$out = 'yes';
					}
				}
			}
			
			$arr = [
				'Января',
				'Февраля',
				'Марта',
				'Апреля',
				'Мая',
				'Июня',
				'Июля',
				'Августа',
				'Сентября',
				'Октября',
				'Ноября',
				'Декабря'
			];
			
			if (($dtd < $dtn) && ( $dtd != '99.99' ) && ( $dtn != '99.99' )) {
				$time      = strtotime($dtd . '.'. date("Y"));
				$newformat = date('j',$time);
				$month     = date('n',$time) - 1;				
				
				echo '<div class="not-v-salone">Будет '. $newformat . ' ' . $arr[$month] .' с 9 часов</div>';
			} else if (($dtd > $dtn) && ( $dtd != '99.99' ) && ( $dtn != '99.99' )) {
				$time = strtotime($dtn . '.' . date("Y"));
				$newformat = date('j',$time);
				$month     = date('n',$time) - 1;
				
				echo '<div class="not-v-salone">Будет '. $newformat . ' ' . $arr[$month] .' с 9 часов</div>';
			}
		}
		
		if ($out     == 'no') {
			echo '<div class="not-v-salone">Расписания пока нет</div>';
		}
		
		$myvariable = ob_get_clean();
		return $myvariable;
	}
	
	add_shortcode( 'calendar-count', 'rmcc_calendar_count_parameters_shortcode' );
	function rmcc_calendar_count_parameters_shortcode( $atts ) {
		ob_start();
		
		$hours = date("H");
		
		$spa_mon    = get_option('spa_mon',  '');
    	$spa_night  = get_option('spa_night', '');	
		$cal   = '';
		$masters = [];
		
		if (($hours >= $spa_mon) && ($hours < $spa_night)) {
			$cal = get_option('cal_d', '');
		} else {
			$cal = get_option('cal_n', '');
		}
		
		foreach ($cal[date('d.m', strtotime("+0 days -".$spa_mon." hours"))] as $key => $value) {
			$masters[] = $key;
		}
		
		if (!empty($masters)) {
			
			$sum = (string)count($masters);

			$count = count($masters)%100;

			if ($count/10 >= 2 || $count < 10) {
				$count %= 10;
				if ($count>4) $tmaster = " Мастеров";
				elseif ($count>1) $tmaster = " Мастера";
				elseif ($count>0) $tmaster = " Мастер";
				else $tmaster = " Мастеров";
			} elseif ($count >= 10 && $count < 21 ) {
				$tmaster = " Мастеров";
			}

			echo count($masters) . $tmaster;
			
		} else {
			echo " наши мастера. Пожалуйста, ознакомьтесь с полным каталогом во вкладке \"Все\"";
		}
		$myvariable = ob_get_clean();
		return $myvariable;
	}
	
	add_shortcode( 'calendar-smolnii-count', 'rmcc_calendar_count_parameters_shortcode2' );
	function rmcc_calendar_count_parameters_shortcode2( $atts ) {
		ob_start();
		
		$hours = date("H");
		
		$spa_smolnii_mon    = get_option('spa_smolnii_mon',  '');
    	$spa_smolnii_night  = get_option('spa_smolnii_night', '');	
		$cal   = '';
		$masters = [];
		
		if (($hours >= $spa_smolnii_mon) && ($hours < $spa_smolnii_night)) {
			$cal = get_option('cal_smolnii_d', '');
		} else {
			$cal = get_option('cal_smolnii_n', '');
		}
		
		foreach ($cal[date('d.m', strtotime("+0 days -".$spa_smolnii_mon." hours"))] as $key => $value) {
			$masters[] = $key;
		}
		
		if (!empty($masters)) {
			
			$sum = (string)count($masters);

			$count = count($masters)%100;

			if ($count/10 >= 2 || $count < 10) {
				$count %= 10;
				if ($count>4) $tmaster = " Мастеров";
				elseif ($count>1) $tmaster = " Мастера";
				elseif ($count>0) $tmaster = " Мастер";
				else $tmaster = " Мастеров";
			} elseif ($count >= 10 && $count < 21 ) {
				$tmaster = " Мастеров";
			}

			echo count($masters) . $tmaster;
			
		} else {
			echo " наши мастера. Пожалуйста, ознакомьтесь с полным каталогом во вкладке \"Все\"";
		}
		$myvariable = ob_get_clean();
		return $myvariable;
	}

	add_shortcode( 'calendar-posts', 'rmcc_post_listing_parameters_shortcode' );
	function rmcc_post_listing_parameters_shortcode( $atts ) {
		ob_start();

		// define attributes and their defaults
		extract( shortcode_atts( array (
			'type'    => 'master',
			'order'   => 'ASC',
			'orderby' => 'name',
			'posts'   => -1
		), $atts ) );
		
		$hours =  date("H"); 
		$spa_mon    = get_option('spa_mon',  '');
    	$spa_night  = get_option('spa_night', '');	
		$cal   = '';
		$masters = [];

		if (($hours >= $spa_mon) && ($hours < $spa_night)) {
			$cal = get_option('cal_d', '');
		} else {
			$cal = get_option('cal_n', '');
		}
		foreach ($cal[date('d.m', strtotime("+0 days -".$spa_mon." hours"))] as $key => $value) {
			$masters[] = $key;
		}
		if (!empty($masters)) {
	  
			// define query parameters based on attributes
			$options = array(
				'post_type'      => $type,
				'post_status'    => 'publish',
				'order'          => $order,
				'orderby'        => $orderby,
				'posts_per_page' => $posts,
				'post__in'        => $masters
			);
			
			$query = new WP_Query($options); 
			if($query->have_posts()) : 

				while($query->have_posts()) : 
					
					$query->the_post(); 
			?>

					<div class="div-block-538">
						<div class="div-block-604 w-clearfix">
							<div class="text-block-238"><?php the_title(); ?></div>
							<div class="div-block-605">
								<div class="text-block-273">Сейчас в салоне</div>
								<div class="div-block-606">
								</div>
							</div>
						</div>
						<div class="div-block-501" data-ix="strelki-slaydera">
							<div data-animation="cross" data-nav-spacing="5" data-duration="500" data-infinite="1" class="slider-10 w-slider" data-ix="strelki-slaydera">
								<?php if( have_rows('galereya') ){ ?>
								<div class="w-slider-mask">
									<?php 
									global $parent_id;
									$parent_id = $loop_id; 
									$loop_index = 0; 
									$loop_field = 'galereya'; 
									while( have_rows('galereya') ){ 
										global $loop_id; $loop_index++; 
										$loop_id++; 
										the_row(); 
									?>
									<div class="slide-11 w-slide">
										<div class="div-block-539" style="background-image:url('<?php 

											$field = get_sub_field('foto'); 

											if(isset($field['url'])) {
												echo($field['url']); 
											} elseif (is_numeric($field)) {
												echo (wp_get_attachment_image_url($field, 'full')); 
											} else { 
												echo($field); 
											} 
																								?>');">
										</div>
									</div>
									<?php } ?>
								</div>
								<?php } ?>
								<div class="left-arrow-5 _524 w-slider-arrow-left">
									<div class="icon-8 w-icon-slider-left">
									</div>
								</div>
								<div class="left-arrow-5 w-slider-arrow-right">
									<div class="icon-7 w-icon-slider-right">
									</div>
								</div>
								<div class="slide-nav-7 w-slider-nav w-round">
								</div>
							</div>
							<a href="<?php the_permalink(); ?>" class="link-block-5 w-inline-block"></a>
							<div class="div-block-528">
								<div class="text-block-66-copy new"><?= get_field('lejbl_new') ?></div>
								<div class="text-block-66-copy"><?= get_field('lejbl_top') ?></div>
							</div>
						</div>
						<div class="div-block-601">
							<div class="div-block-602">
								<div class="div-block-603">
									<div class="text-block-272">20
										<br/>лет
									</div>
								</div>
								<div class="div-block-603">
									<div class="text-block-272">163
										<br/>см
									</div>
								</div>
								<div class="div-block-603">
									<div class="text-block-272">3-ий
										<br/>размер
									</div>
								</div>
							</div>
							<div class="div-block-725"><?= get_field('raspisanie_v_obschem_spiske') ?></div>
							<a href="<?php the_permalink(); ?>" class="text-block-195 _56844">Страница мастера</a>
						</div>
					</div>

			<?php 
				endwhile; 
			endif; 
			wp_reset_postdata(); 

		}
		$myvariable = ob_get_clean();
		return $myvariable;
	}

	add_shortcode( 'calendar-smolnii-posts', 'rmcc_post_listing_parameters_shortcode1' );
	function rmcc_post_listing_parameters_shortcode1( $atts ) {
		ob_start();

		// define attributes and their defaults
		extract( shortcode_atts( array (
			'type'    => 'master',
			'order'   => 'ASC',
			'orderby' => 'name',
			'posts'   => -1
		), $atts ) );
		
		$hours =  date("H"); 
		$spa_mon    = get_option('spa_smolnii_mon',  '');
    	$spa_night  = get_option('spa_smolnii_night', '');	
		$cal   = '';
		$masters = [];

		if (($hours >= $spa_mon) && ($hours < $spa_night)) {
			$cal = get_option('cal_smolnii_d', '');
		} else {
			$cal = get_option('cal_smolnii_n', '');
		}
		foreach ($cal[date('d.m', strtotime("+0 days -".$spa_mon." hours"))] as $key => $value) {
			$masters[] = $key;
		}
		if (!empty($masters)) {
	  
			// define query parameters based on attributes
			$options = array(
				'post_type'      => $type,
				'post_status'    => 'publish',
				'order'          => $order,
				'orderby'        => $orderby,
				'posts_per_page' => $posts,
				'post__in'        => $masters
			);
			
			$query = new WP_Query($options); 
			if($query->have_posts()) : 

				while($query->have_posts()) : 
					
					$query->the_post(); 
			?>

					<div class="div-block-538">
						<div class="div-block-604 w-clearfix">
							<div class="text-block-238"><?php the_title(); ?></div>
							<div class="div-block-605">
								<div class="text-block-273">Сейчас в салоне</div>
								<div class="div-block-606">
								</div>
							</div>
						</div>
						<div class="div-block-501" data-ix="strelki-slaydera">
							<div data-animation="cross" data-nav-spacing="5" data-duration="500" data-infinite="1" class="slider-10 w-slider" data-ix="strelki-slaydera">
								<?php if( have_rows('galereya') ){ ?>
								<div class="w-slider-mask">
									<?php 
									global $parent_id;
									$parent_id = $loop_id; 
									$loop_index = 0; 
									$loop_field = 'galereya'; 
									while( have_rows('galereya') ){ 
										global $loop_id; $loop_index++; 
										$loop_id++; 
										the_row(); 
									?>
									<div class="slide-11 w-slide">
										<div class="div-block-539" style="background-image:url('<?php 

											$field = get_sub_field('foto'); 

											if(isset($field['url'])) {
												echo($field['url']); 
											} elseif (is_numeric($field)) {
												echo (wp_get_attachment_image_url($field, 'full')); 
											} else { 
												echo($field); 
											} 
																								?>');">
										</div>
									</div>
									<?php } ?>
								</div>
								<?php } ?>
								<div class="left-arrow-5 _524 w-slider-arrow-left">
									<div class="icon-8 w-icon-slider-left">
									</div>
								</div>
								<div class="left-arrow-5 w-slider-arrow-right">
									<div class="icon-7 w-icon-slider-right">
									</div>
								</div>
								<div class="slide-nav-7 w-slider-nav w-round">
								</div>
							</div>
							<a href="<?php the_permalink(); ?>" class="link-block-5 w-inline-block"></a>
							<div class="div-block-528">
								<div class="text-block-66-copy new"><?= get_field('lejbl_new') ?></div>
								<div class="text-block-66-copy"><?= get_field('lejbl_top') ?></div>
							</div>
						</div>
						<div class="div-block-601">
							<div class="div-block-602">
								<div class="div-block-603">
									<div class="text-block-272">20
										<br/>лет
									</div>
								</div>
								<div class="div-block-603">
									<div class="text-block-272">163
										<br/>см
									</div>
								</div>
								<div class="div-block-603">
									<div class="text-block-272">3-ий
										<br/>размер
									</div>
								</div>
							</div>
							<div class="div-block-725"><?= get_field('raspisanie_v_obschem_spiske') ?></div>
							<a href="<?php the_permalink(); ?>" class="text-block-195 _56844">Страница мастера</a>
						</div>
					</div>

			<?php 
				endwhile; 
			endif; 
			wp_reset_postdata(); 

		}
		$myvariable = ob_get_clean();
		return $myvariable;
	}
	
	function my_acf_load_value($value, $post_id, $field) {
		if (is_admin()) {
			// don't do this in the admin
			// could have unintended side effects
			return $value;
		}
		$value = apply_filters('the_content',$value); 

    	return $value;
	}

	// acf/load_value/name={$field_name} - filter for a value load based on it's field type
	add_filter('acf/load_value/name=mastera_v_salone', 'my_acf_load_value', 10, 3);
	add_filter('acf/load_value/name=mastera_v_salone_smolnyj', 'my_acf_load_value', 10, 3);
	add_filter('acf/load_value/name=kolichestvo_masterov', 'my_acf_load_value', 10, 3);
	add_filter('acf/load_value/name=kolichestvo_masterov_smolnyj', 'my_acf_load_value', 10, 3);
	add_filter('acf/load_value/name=metka_v_salone', 'my_acf_load_value', 10, 3);
	add_filter('acf/load_value/name=sejchas_v_salone_v_obschem_spiske_nad_foto', 'my_acf_load_value', 10, 3);
	add_filter('acf/load_value/name=raspisanie_v_obschem_spiske', 'my_acf_load_value', 10, 3);
	add_filter('acf/load_value/name=raspisanie_na_stranice_mastera', 'my_acf_load_value', 10, 3);
	add_filter('acf/load_value/name=pole_dlya_tablicy_raspisaniya', 'my_acf_load_value', 10, 3);
	
});

/*
 * Подключение игр
*/
require_once 'games/init.php';














?>