<h1>Страница настроек расписания сотрудников</h1>

<form method="POST">
	<input type="hidden" name="calendar_spa" value="calendar">

	<h3>График работ</h3>
	
	<?php if ($saved) { ?>
		<div class="alert alert-info alert-dismissible">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Сохранено!</strong>
		</div>
	<?php } ?>
	
	<table class="tg">
		<tr>
			<th class="tg-obcv" rowspan="3">Мастера/День</th>
			<th class="tg-qtf5" colspan="2"><?php echo date('d.m', strtotime("+0 days")); ?></th>
			<th class="tg-73oq" colspan="2"><?php echo date('d.m', strtotime("+1 days")); ?></th>
			<th class="tg-73oq" colspan="2"><?php echo date('d.m', strtotime("+2 days")); ?></th>
			<th class="tg-73oq" colspan="2"><?php echo date('d.m', strtotime("+3 days")); ?></th>
			<th class="tg-73oq" colspan="2"><?php echo date('d.m', strtotime("+4 days")); ?></th>
			<th class="tg-73oq" colspan="2"><?php echo date('d.m', strtotime("+5 days")); ?></th>
			<th class="tg-73oq" colspan="2"><?php echo date('d.m', strtotime("+6 days")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+7 days")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+8 days")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+9 days")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+10 days")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+11 days")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+12 days")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+13 days")); ?></th>
		</tr>
		<tr>
			<td class="tg-obcv">д</td>
			<td class="tg-obcv">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-73oq">н</td>
		</tr>
		<tr>
			<td class="tg-obcv" colspan="2"><?php echo getDayRus("0"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("1"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("2"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("3"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("4"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("5"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("6"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("7"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("8"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("9"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("10"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("11"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("12"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("13"); ?></td>
		</tr>
		
		<?php
			$args = array(
				'post_type' => 'master',
				'post_status'    => 'publish',
				'orderby'        => 'name',
				'order'          => 'ASC',
				'posts_per_page' => '500',
				'tax_query' => array( 
					array(
						'taxonomy' => 'master-cat',
						'field'    => 'slug',
						'terms'    => array('rabotayut')
					)
				)
			);
			$posts = new WP_Query( $args );
			
			$num = 1;
			if ( $posts->have_posts() ) : 
				while ( $posts->have_posts() ) : 
					$posts->the_post();	
					if ($num == 1) {
		?>
			
			<tr>
				<td class="tg-cum3"><?php the_title(); ?></td>
				
				<?php for ($i = 0; $i<28; $i++) { ?>
				
					<?php
						$checked = false;
						
						if ($i % 2 == 0) {
							$checked = $cal_d[date('d.m', strtotime("+". intval($i / 2) ." days"))][get_the_ID()] == 'on' ? true : false;
						} else {
							$checked = $cal_n[date('d.m', strtotime("+". intval($i / 2) ." days"))][get_the_ID()] == 'on' ? true : false;
						}
					?>
				
					<td class="tg-j3py tg-pd0">
						<label class="container">
							<input name="cal_<?php echo ($i % 2 == 0) ? 'd' : 'n'; ?>[<?php echo date('d.m', strtotime("+". intval($i / 2) ." days")); ?>][<?php echo get_the_ID(); ?>]" type="checkbox" <?php echo ($checked == true) ? 'checked' : '' ?>>
							<span class="checkmark"></span>
						</label>
					</td>
					
				<?php } ?>

			</tr>
		
		<?php	
			$num = 2;
					} else { 
		?>
		
			<tr>
				<td class="tg-cum3"><?php the_title(); ?></td>
				
				<?php for ($i = 0; $i<28; $i++) { ?>
				
					<?php
						$checked = false;
						
						if ($i % 2 == 0) {
							$checked = $cal_d[date('d.m', strtotime("+". intval($i / 2) ." days"))][get_the_ID()] == 'on' ? true : false;
						} else {
							$checked = $cal_n[date('d.m', strtotime("+". intval($i / 2) ." days"))][get_the_ID()] == 'on' ? true : false;
						}
					?>
				
					<td class="tg-73oq tg-pd0">
						<label class="container">
							<input name="cal_<?php echo ($i % 2 == 0) ? 'd' : 'n'; ?>[<?php echo date('d.m', strtotime("+". intval($i / 2) ." days")); ?>][<?php echo get_the_ID(); ?>]" type="checkbox" <?php echo ($checked == true) ? 'checked' : '' ?>>
							<span class="checkmark"></span>
						</label>
					</td>
					
				<?php } ?>
				
			</tr>
		
		<?php
			$num = 1; 
					}
				endwhile;  
				wp_reset_query(); 
			endif;   
		?>
		
	</table>
	<br>
	
    <input type="submit" value="Сохранить" class="button button-primary button-large">
</form>