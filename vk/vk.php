<?php
/**
 * Make an API call to https://api.vk.com/method/
 * @return string The response, decoded from json format
 */
function api($method, array $query = array(), $accessToken)
{
    /* Generate query string from array */
    $parameters = array();
    foreach ($query as $param => $value) {
        $q = $param . '=';
        if (is_array($value)) {
            $q .= urlencode(implode(',', $value));
        } else {
            $q .= urlencode($value);
        }
        $parameters[] = $q;
    }
    $q = implode('&', $parameters);
    if (count($query) > 0) {
        $q .= '&'; // Add "&" sign for access_token if query exists
    }
    $url = 'https://api.vk.com/method/' . $method . '?' . $q . 'access_token=' . $accessToken;
    //var_dump( $url );
    $result = json_decode( curl($url) );
    if (isset($result->response)) {
        return $result->response;
    }
    return $result;
}

/**
 * Make the curl request to specified url
 * @param string $url The url for curl() function
 * @return mixed The result of curl_exec() function
 * @throws \Exception
 */
function curl($url, $data = array())
{
    // create curl resource
    $ch = curl_init();
    // set url
    curl_setopt($ch, CURLOPT_URL, $url);
    
    if (count( $data ) > 0) {
        
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array(
            'photo' => new CurlFile($data),
        ));
        
    }
    
    // return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    // disable SSL verifying
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    // $output contains the output string
    $result = curl_exec($ch);
    if (!$result) {
        $errno = curl_errno($ch);
        $error = curl_error($ch);
    }
    // close curl resource to free up system resources
    curl_close($ch);
    if (isset($errno) && isset($error)) {
        throw new \Exception($error, $errno);
    }
    return $result;
}

function do_vk_post()
{
    $accessToken = get_option('vk_token', ''); 
	
	$vk = new Vk($accessToken);
	
	//var_dump( $accessToken );
    
    if ($accessToken === ''){
        return;
    }
    
    $VERSION = '5.103';
    
    $data  = [];
    $photo = [];
    $text_master = '';
    
    $hours =  gmdate("H", time() + 3600 * get_option('spa_zone', '+3')); // getdate();	
	$spa_mon    = get_option('spa_mon',  '9');
	$spa_night  = get_option('spa_night', '21');	
	$vk_group   = get_option('vk_group', '');
    $cal   = '';
    $masters = [];
    
    $now_day = true;
    
    if (($hours >= $spa_mon) && ($hours < $spa_night)) {
    	$cal = get_option('cal_d', '');
    	$now_day = true;
    } else {
    	$cal = get_option('cal_n', '');
    	$now_day = false;
    }
    
    foreach ($cal[date('d.m', strtotime("+0 days"))] as $key => $value) {
    	$masters[] = $key;
    }
    
    if (!empty($masters)) {
        
        foreach ($masters as $key => $value) {
        	if( have_rows('galereya_vk', $value) ) {
        	    
        	    $images = [];
        	    
                while( have_rows('galereya_vk', $value) ) {
                    the_row(); 
                    
                    $attachment = get_sub_field('foto_vk');
                    $image = get_attached_file( $attachment["ID"], "full" ); 
                    
                    $images[] = $image;
                }
                
                //var_dump( $images );
                
                $data['images'][] = $images[ array_rand($images) ];
            }
        }
        
        $args = array(
    		'post_type'      => 'master',
    		'post_status'    => 'publish',
    		'order'          => 'ASC',
    		'orderby'        => 'name',
    		'posts_per_page' => -1,
    		'post__in'       => $masters
    	);
    		
        $posts = new WP_Query( $args );
        
        if ( $posts->have_posts() ) { 
        	while ( $posts->have_posts() ) { 
        		$posts->the_post();	
        	
        	    $data['masters'][] = get_the_title();
        	    $data['links'][]   = get_the_permalink();
        	    
        	    //var_dump( get_the_title() );
        	}
        	
        	wp_reset_query(); 
        }
        
        $sum = (string)count($masters);
    	$tmaster = " мастеров";
    	
    	if (count($masters) > 9) {
    		
    		if ($sum[strlen($sum)-1] == '2' || 
    			$sum[strlen($sum)-1] == '3' ||
    			$sum[strlen($sum)-1] == '4' 
    			) 
    		{
    			$tmaster = " мастера";
    		} else if ($sum[strlen($sum)-1] == '1') {
    			$tmaster = " мастер";
    		} else {
    			$tmaster = " мастеров";
    		}
    		
    	} else {
    		if ($sum[0] == '2' || 
    			$sum[0] == '3' || 
    			$sum[0] == '4') 
    		{
    			$tmaster = " мастера";
    		} else if (($sum[strlen($sum)-1] == '5') || ($sum[strlen($sum)-1] == '6') || ($sum[strlen($sum)-1] == '7') || ($sum[strlen($sum)-1] == '8') || ($sum[strlen($sum)-1] == '9')) {
    			$tmaster = " мастеров";
    		} else {
    			$tmaster = " мастер";
    		}
    	}
    
    	$text_master = count($masters) . $tmaster;
    } else {
        return;
    }
    
    if ( count( $data['images'] ) > 6) {
        
        $index = 5; // your index here
        array_splice($data['images'], $index + 1, count( $data['images'] ) - ($index + 1) );
    }
    
    /*$response = api('photos.getWallUploadServer', [
            'group_id' => $vk_group,
            'v'        => $VERSION
        ], $accessToken);*/
	
	$response = $vk->photosGetWallUploadServer($vk_group);
	
	//var_dump( $response );
        
    $uploadURL = $response['upload_url'];
	
	$arrimages = array_filter($data['images']);
	
	//var_dump( $arrimages );
    
    foreach ($arrimages as $image) {
        
        /*$output  =  curl($uploadURL,  $image);
        
        $response = json_decode($output);*/
		
		$response = $vk->uploadFile($uploadURL, $image);
		
		//var_dump( $output );
		//var_dump( $response->photo );
        
        /*$response = api('photos.saveWallPhoto', [
                    'group_id' => $vk_group,
                    'photo'    => stripslashes($response->photo),
                    'server'   => $response->server,
                    'hash'     => $response->hash,
                    'v'        => $VERSION,
                ], $accessToken);*/
		
		$response = $vk->photosSaveWallPhoto([
				'group_id' => $vk_group,
				'photo' => $response['photo'],
				'server' => $response['server'],
				'hash' => $response['hash']
			]
		);
        
        $photo[] = "photo{$response[0]['owner_id']}_{$response[0]['id']}";
    }
	
	//var_dump( $photo );
    
    $vk_desc_d  = get_option('vk_desc_d', '');
    $vk_desc_n  = get_option('vk_desc_n', '');
    $vk_cont_desc  = get_option('vk_cont_desc', '');
    $vk_phone   = get_option('vk_phone', '');
    $vk_adress  = get_option('vk_adress', '');
    $vk_link    = get_option('vk_link', '');
    $vk_tags    = get_option('vk_tags', '');
    $vk_smile   = get_option('vk_smile', '');
    
    $message = '';
    
    if (($hours > $spa_mon) && ($hours < $spa_night)) {
    	$message .= str_replace("{spa}", $text_master, $vk_desc_d);
    } else {
        $message .= str_replace("{spa}", $text_master, $vk_desc_n);
    }
    $message .= "\n\n";
    
    foreach ($data['masters'] as $key => $value) {
    	
    	$message .= $vk_smile . ' ' . $data['masters'][$key] . ' - ' . $data['links'][$key];
        $message .= "\n";
    }
    $message .= "\n\n";
    
    $message .= $vk_phone;
    $message .= "\n";
    $message .= $vk_adress;
    $message .= "\n";
    $message .= $vk_link;
    $message .= "\n\n";
    
    $new_arr = array_map('trim', explode(',', $vk_tags));
    
    foreach ($new_arr as $tag) {
        $message .= ' #' . $tag;
    }
    
    $response = api('wall.post', 
                [
                    'owner_id' => "-" . $vk_group,
                    'v'        => $VERSION,
                    'from_group' => 1,
                    'message' => $message,
                    'attachments' => implode(',', $photo), // uploaded image is passed as attachment
                ], $accessToken);
    
    if ( $now_day ) {
        update_option('vk_saved_d', $response->post_id);
        
        $vk_saved_n = get_option('vk_saved_n', ''); 
    
        if ($vk_saved_n !== ''){
            $response = api('wall.delete', 
                [
                    'owner_id' => "-" . $vk_group,
                    'v'        => $VERSION,
                    'post_id'  => $vk_saved_n
                ], $accessToken);
        }
    } else {
        update_option('vk_saved_n', $response->post_id);
        
        $vk_saved_d = get_option('vk_saved_d', ''); 
    
        if ($vk_saved_d !== ''){
            $response = api('wall.delete', 
                [
                    'owner_id' => "-" . $vk_group,
                    'v'        => $VERSION,
                    'post_id'  => $vk_saved_d
                ], $accessToken);
        }
    }
}

class Vk
{
    private $token;
    private $v = '5.37';

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function wallPost($data)
    {
        return $this->request('wall.post', $data);
    }

    public function photosGetWallUploadServer($group_id)
    {
        $params = [
            'group_id' => $group_id,
        ];
        return $this->request('photos.getWallUploadServer', $params);
    }

    /**
     * @param $params [user_id, group_id, photo, server, hash]
     * @return mixed
     * @throws \Exception
     */
    public function photosSaveWallPhoto($params)
    {
        return $this->request('photos.saveWallPhoto', $params);
    }

    public function uploadFile($url, $path)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);

        if (class_exists('\CURLFile')) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, ['file1' => new \CURLFile($path)]);
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, ['file1' => "@$path"]);
        }

        $data = curl_exec($ch);
        curl_close($ch);
        return json_decode($data, true);
    }

    private function request($method, array $params)
    {
        $params['v'] = $this->v;

        $ch = curl_init('https://api.vk.com/method/' . $method . '?access_token=' . $this->token);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $data = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($data, true);
        if (!isset($json['response'])) {
            throw new \Exception($data);
        }
        usleep(mt_rand(1000000, 2000000));
        return $json['response'];
    }
}



