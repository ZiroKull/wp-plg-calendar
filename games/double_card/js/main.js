$(document).ready(function() 
{
    var gameStart = false;
    var gameEnd   = false;
    var cardOpend = false;
    var time      = 90; 
    var par       = 0;
    var fImg      = '';
    var sImg      = '';
    var fCard     = false;
    var click     = 0;
    var fObj, sObj;
    
    var x = setInterval(function() {
    
        if (gameStart && !gameEnd) {
            $(".text-block-348:eq(0) strong").html(time-- + ' сек');
            
            if (time < 0) {
                
                gameStart = false;
                gameEnd   = true;
                
                $(".game-over").css({
                    'display' : '-webkit-box',
                    'display' : '-webkit-flex',
                    'display' : '-ms-flexbox',
                    'display' : 'flex',
                });
                
                //clearInterval(x);
            }
        }

    }, 1000);
    
    $(document).on('click', '.close-game-over, .close-user-win', function(){
        $(".game-over").css('display', 'none');
        $(".user-win").css('display', 'none');
    });
    
    $(document).on('click', '.game-repeat', function(){
        
        par       = 0;
        time      = 90;
        fImg      = '';
        sImg      = '';
        click     = 0;
        fCard     = false;
        
        $(".text-block-348:eq(1) strong").html(par + ' из 14');
        $(".text-block-348:eq(0) strong").html(time + ' сек');
        
        $.post( myajax.url, {'action': 'rgame'}, function(response) {
            
            $('.cart-wrap').html( response );
            
            $(".game-over").css('display', 'none');
            
            gameStart = true;
            gameEnd   = false;
            
		});
    });
    
    $(document).on('click', '.card.div-block-809:not(.opend):not(.selected)', function(){
        
        if ($(this).hasClass('opend'))
            return;
        
        if (cardOpend)
            return;
            
        if (gameEnd)
            return;
            
        if (click >= 2)
            return;
       
        if (!gameStart)
            gameStart = true;
            
        $(this).css({
            'transition' : 'transform 0.2s ease 0s, transform 200ms ease 0s',
            'transform' : 'translateX(0px) translateY(0px) rotateX(0deg) rotateY(180deg) rotateZ(0deg)',
            'transform-style' : 'preserve-3d'
        });
            
        if (!fCard) {
            
            fObj  = $(this);
            fCard = true;
            fImg  = $(this).find('.avers').css('background-image');
            fObj.addClass('selected');
            fImg  = fImg.replace(/(url\(|\)|")/g, '');
            
        } else {
            
            sObj  = $(this);
            fCard = false;
            sImg  = $(this).find('.avers').css('background-image');
            sObj.addClass('selected');
            sImg  = sImg.replace(/(url\(|\)|")/g, '');
            
        }
        
        click++;
        
        if (click >= 2) {
            cardOpend = true;
            
            if (fImg === sImg) {
                
                click = 0;
                opnedCart( fObj, sObj );
            } else {

                closeCart( fObj, sObj );
            }
        }
       
    });
    
    function closeCart(obj1, obj2) {
        
        setTimeout(function(){
            
            click = 0;
            
            obj1.attr('style', 'transition: transform 0.2s ease 0s, transform 200ms ease 0s;transform-style: preserve-3d;');
            
            obj2.attr('style', 'transition: transform 0.2s ease 0s, transform 200ms ease 0s;transform-style: preserve-3d;');
            
            obj1.removeClass('selected');
            obj2.removeClass('selected');
            fCard = false;
            
            cardOpend = false;
            
            /*setTimeout(function(){
                cardOpend = false;
            }, 500);*/
                
        }, 1000);
    }
    
    function opnedCart(obj1, obj2) {
        par = par + 1;
        $(".text-block-348:eq(1) strong").html(par + ' из 14');
        obj1.addClass('opend');
        obj2.addClass('opend');
        fCard = false;
        
        cardOpend = false;
		
		console.log(par);
        
        if (par == 14) {
            $(".user-win").css({
                'display' : '-webkit-box',
                'display' : '-webkit-flex',
                'display' : '-ms-flexbox',
                'display' : 'flex',
            });
            
            gameStart = false;
            gameEnd   = true;
        }
    }
    
});