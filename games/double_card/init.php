<?php
    // game source
    add_action('wp_enqueue_scripts','game_init');

    function game_init() {
        wp_enqueue_script( 'ava-test-js', plugins_url( '/js/main.js', __FILE__ ), array(), time(), true);
        
        wp_localize_script( 'ava-test-js', 'myajax', 
    		array(
    			'url' => admin_url('admin-ajax.php')
    		)
    	);
    }
    
    
    function my_acfg_load_value($value, $post_id, $field) {
		if (is_admin()) {
			// don't do this in the admin
			// could have unintended side effects
			return $value;
		}
		$value = apply_filters('the_content',$value); 

    	return $value;
	}
	
	// acf/load_value/name={$field_name} - filter for a value load based on it's field type
	add_filter('acf/load_value/name=kod_dlya_igry', 'my_acfg_load_value', 10, 3);
	
	add_shortcode( 'games-doublecard', 'games_doublecard' );	
	function games_doublecard( $atts ) {
		ob_start();
		
		$images = [];
		
		if( have_rows('igra', 'options') ) {
            while( have_rows('igra', 'options') ) {
                the_row(); 
                
                $attachment = get_sub_field('foto_dlya_kartochki');
                $image = wp_get_attachment_image_url( $attachment["ID"], "full" ); 
                
                $images[] = $image;
            }
        }
        
        shuffle( $images );
        
        $data["images"] = [];
        
        for ($i = 0; $i < 14; $i++) {
            $data["images"][] = $images[ $i ];
        }
        
        $data["images"] = array_merge($data["images"], $data["images"]) ;
        
        shuffle( $data["images"] );
        
        for ($i = 0; $i < 28; $i++) {
            
		?>
		
		    <div class="card-wraper">
                <div class="card div-block-809">
                    <div class="avers" style="background-image:url('<?= $data["images"][$i] ?>');"></div>
                    <div class="revers"></div>
                </div>
            </div>
		
		<?php
		
        }
        
        $attachment = get_field('pobeda_foto', 'options');
        $image = wp_get_attachment_image_src( $attachment["ID"], "full" ); 
        
        ?>
        
        
        <div class="pop-vozrast game-over" style="display: none;">
            <div class="div-block-812">
                <h1 class="heading-37"><?= the_field('vremya_vyshlo', 'options'); ?></h1>
                <p class="paragraph-59"><?= the_field('vremya_vyshlo_opisanie', 'options'); ?></p>
                <a href="#" class="button-29 w-button game-repeat">Сыграем ещё раз?</a>
                <div class="div-block-519 close-game-over"></div>
                <div class="text-block-356">Поиграл сам - поделись с другом!</div>
                <div class="div-block-831"><?= the_field('kod_podelitsya', 'options'); ?></div>
            </div>
        </div>
        
        <div class="pop-vozrast user-win" style="display: none;">
            <div class="div-block-812 _58544444">
              <div class="div-block-519  close-user-win">
              </div>
              <div class="div-block-814" style="background-image:url('<?= $image[0]; ?>');">
              </div>
              <div class="div-block-813">
                <h1 class="heading-39"><?= the_field('pobeda', 'options'); ?></h1>
                <p class="paragraph-59"><?= the_field('pobeda_opisanie', 'options'); ?></p>
                <a href="<?= the_field('pobeda_knopka', 'options'); ?>" class="button-29 w-button">О салоне "Premier Spa"</a>
                <div class="text-block-356">Поиграл сам - поделись с другом!</div>
                <div class="div-block-831"><?= the_field('kod_podelitsya', 'options'); ?></div>
              </div>
            </div>
        </div>
        
        
        <?php
        
		
		$myvariable = ob_get_clean();
		return $myvariable;
	}
	
	add_action('wp_ajax_rgame', 'game_repeat');
    add_action('wp_ajax_nopriv_rgame', 'game_repeat');
    
    function game_repeat(){
        
        
        ob_start();
		
		$images = [];
		
		if( have_rows('igra', 'options') ) {
            while( have_rows('igra', 'options') ) {
                the_row(); 
                
                $attachment = get_sub_field('foto_dlya_kartochki');
                $image = wp_get_attachment_image_url( $attachment["ID"], "full" ); 
                
                $images[] = $image;
            }
        }
        
        shuffle( $images );
        
        $data["images"] = [];
        
        for ($i = 0; $i < 14; $i++) {
            $data["images"][] = $images[ $i ];
        }
        
        $data["images"] = array_merge($data["images"], $data["images"]) ;
        
        shuffle( $data["images"] );
        
        for ($i = 0; $i < 28; $i++) {
            
		?>
		
		    <div class="card-wraper">
                <div class="card div-block-809">
                    <div class="avers" style="background-image:url('<?= $data["images"][$i] ?>');"></div>
                    <div class="revers"></div>
                </div>
            </div>
		
		<?php
		
		$attachment = get_field('pobeda_foto', 'options');
        $image = wp_get_attachment_image_src( $attachment["ID"], "full" ); 
		
		?>
        
        
        <div class="pop-vozrast game-over" style="display: none;">
            <div class="div-block-812">
                <h1 class="heading-37"><?= the_field('vremya_vyshlo', 'options'); ?></h1>
                <p class="paragraph-59"><?= the_field('vremya_vyshlo_opisanie', 'options'); ?></p>
                <a href="#" class="button-29 w-button game-repeat">Сыграем ещё раз?</a>
                <div class="div-block-519 close-game-over"></div>
                <div class="text-block-356">Поиграл сам - поделись с другом!</div>
                <div class="div-block-831"><?= the_field('kod_podelitsya', 'options'); ?></div>
            </div>
        </div>
        
        <div class="pop-vozrast user-win" style="display: none;">
            <div class="div-block-812 _58544444">
              <div class="div-block-519  close-user-win">
              </div>
              <div class="div-block-814" style="background-image:url('<?= $image[0]; ?>');">
              </div>
              <div class="div-block-813">
                <h1 class="heading-39"><?= the_field('pobeda', 'options'); ?></h1>
                <p class="paragraph-59"><?= the_field('pobeda_opisanie', 'options'); ?></p>
                <a href="<?= the_field('pobeda_knopka', 'options'); ?>" class="button-29 w-button">О салоне "Premier Spa"</a>
                <div class="text-block-356">Поиграл сам - поделись с другом!</div>
                <div class="div-block-831"><?= the_field('kod_podelitsya', 'options'); ?></div>
              </div>
            </div>
        </div>
        
        
        <?php
		
        }
		
		$myvariable = ob_get_clean();
		echo $myvariable;
        
        
        wp_die();
    }
    