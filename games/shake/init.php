<?php
    // game source
    add_action('wp_enqueue_scripts','shake_init');

    function shake_init() {
        wp_enqueue_script( 'shake-js', plugins_url( '/js/main.js', __FILE__ ), array(), time(), true);
        
        wp_localize_script( 'shake-js', 'myajax', 
    		array(
    			'url' => admin_url('admin-ajax.php')
    		)
    	);
    }
    
    
    function my_acfg_shake_load_value($value, $post_id, $field) {
		if (is_admin()) {
			// don't do this in the admin
			// could have unintended side effects
			return $value;
		}
		$value = apply_filters('the_content',$value); 

    	return $value;
	}
	
	// acf/load_value/name={$field_name} - filter for a value load based on it's field type
	add_filter('acf/load_value/name=kod_dlya_igry_stryasi_trusy', 'my_acfg_shake_load_value', 10, 3);
	
	add_shortcode( 'games-shake', 'games_shake' );	
	function games_shake( $atts ) {
		ob_start();
		
		$images = [];
		$textes = [];
		
		if( have_rows('dlya_igry', 'options') ) {
            while( have_rows('dlya_igry', 'options') ) {
                the_row(); 
                
                $attachment = get_sub_field('foto');
                $image = wp_get_attachment_image_url( $attachment["ID"], "full" ); 
                
                $images[] = $image;
				
				$text = get_sub_field('tekst');
				$textes[] = $text;
            }
        }
        
        ?>
        <div class="div-block-818 shake-wrap">
            <div class="div-block-819" style="background-image:url('<?= $images[0] ?>');">
                <div class="text-block-351"><?= $textes[0] ?></div>
            </div>
        </div>
        
        <?php
		
		$myvariable = ob_get_clean();
		return $myvariable;
	}
	
	
	add_action('wp_ajax_shake_game', 'shake_next_image');
    add_action('wp_ajax_nopriv_shake_game', 'shake_next_image');
    
    function shake_next_image(){
        
        ob_start();
		
        $images = [];
		$textes = [];
		
		if( have_rows('dlya_igry', 'options') ) {
            while( have_rows('dlya_igry', 'options') ) {
                the_row(); 
                
                $attachment = get_sub_field('foto');
                $image = wp_get_attachment_image_url( $attachment["ID"], "full" ); 
                
                $images[] = $image;
				
				$text = get_sub_field('tekst');
				$textes[] = $text;
            }
        }
        
        ?>
            <div class="div-block-819" style="background-image:url('<?= $images[ $_POST['id'] ] ?>');">
                <div class="text-block-351"><?= $textes[  $_POST['id']  ] ?></div>
            </div>
        
        <?php
		
		
		$myvariable = ob_get_clean();
		echo $myvariable;
        
        
        wp_die();
    }
    