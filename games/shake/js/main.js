var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

window.addEventListener('devicemotion', motion, false);
window.addEventListener('ondevicemotion', motion, false);

var progress = 0;
var image = 0;
var shangeImg = true;

function motion(e) {
    
	var acc = e.acceleration;
	if(!acc.hasOwnProperty('x')) {
		acc = e.accelerationIncludingGravity;
	}

	if(!acc.x) return;

	//only log if x,y,z > 1
	if(Math.abs(acc.x) >= 1 &&
	Math.abs(acc.y) >= 1 &&
	Math.abs(acc.z) >=1) {
		//console.log('motion', acc);

		progress += 2 * Math.abs(acc.x);
        
		if (progress >= 250 && image != 2 && shangeImg) {
			progress = 0;
			image++;
			shangeImg = false;
			
			$.post( myajax.url, {'action': 'shake_game', 'id': image}, function(response) {
               $('.shake-wrap').html( response ); 
               shangeImg = true;
    		});
		}		
		
	}
}




