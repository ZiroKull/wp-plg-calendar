<?php
    // game source
    add_action('wp_enqueue_scripts','viktorina_init');

    function viktorina_init() {
        wp_enqueue_script( 'viktorina-js', plugins_url( '/js/main.js', __FILE__ ), array(), time(), true);
        
        wp_localize_script( 'viktorina-js', 'myajax', 
    		array(
    			'url' => admin_url('admin-ajax.php')
    		)
    	);
    }
    
    
    function my_acfg_viktorina_load_value($value, $post_id, $field) {
		if (is_admin()) {
			// don't do this in the admin
			// could have unintended side effects
			return $value;
		}
		$value = apply_filters('the_content',$value); 

    	return $value;
	}
	
	// acf/load_value/name={$field_name} - filter for a value load based on it's field type
	add_filter('acf/load_value/name=kod_dlya_igry_viktorina', 'my_acfg_viktorina_load_value', 10, 3);
	
	add_shortcode( 'games-victorina', 'games_victorina' );	
	function games_victorina( $atts ) {
		ob_start();
		
		/*$images = [];
		$textes = [];
		
		if( have_rows('dlya_igry', 'options') ) {
            while( have_rows('dlya_igry', 'options') ) {
                the_row(); 
                
                $attachment = get_sub_field('foto');
                $image = wp_get_attachment_image_url( $attachment["ID"], "full" ); 
                
                $images[] = $image;
				
				$text = get_sub_field('tekst');
				$textes[] = $text;
            }
        }*/
        
        
        
        $blk1_attachment = get_field('foto_na_pervyj_blok', 'options');
        $blk1_image = wp_get_attachment_image_url( $blk1_attachment["ID"], "full" ); 
        
        $blk1_title = get_field('zagolovok_pervyj_blok', 'options');
        $blk1_desc  = get_field('opisanie_pervyj_blok', 'options');
        $blk1_btn   = get_field('knopka_pervyj_blok', 'options');
        $blk1_txt   = get_field('tekst_18', 'options');
        
        ?>
        
        
        <div class="div-block-819 _5474" style="background-image:url('<?= $blk1_image; ?>');">
          <div class="div-block-835-copy">
            <h1 class="heading-40"><?= $blk1_title; ?></h1>
            <div class="text-block-358"><?= $blk1_desc; ?></div>
            <a href="#" class="button-31 w-button victorina-start"><?= $blk1_btn; ?></a>
            <div class="text-block-359"><?= $blk1_txt; ?></div>
          </div>
        </div>
        
        
        <?php
		
		$myvariable = ob_get_clean();
		return $myvariable;
	}
	
	
	add_action('wp_ajax_viktorina_init', 'viktorina_get_veiw');
    add_action('wp_ajax_nopriv_viktorina_init', 'viktorina_get_veiw');
    
    function viktorina_get_veiw(){
        
        ob_start();
		
        $voprosi = [];
        $pravvop = [];   
        $nepvop1 = [];
        $nepvop2 = [];
        //$descsuc = [];
        //$descerr = [];
		
		if( have_rows('dlya_igry_viktorina', 'options') ) {
            while( have_rows('dlya_igry_viktorina', 'options') ) {
                the_row(); 
                
				$v_text = get_sub_field('vopros');
				$voprosi[] = $v_text;
				
				$p_text = get_sub_field('pravilnyj_otvet');
				$pravvop[] = $p_text;
				
				$n1_text = get_sub_field('1_nepravilnyj_otvet');
				$nepvop1[] = $n1_text;
				
				$n2_text = get_sub_field('2_nepravilnyj_otvet');
				$nepvop2[] = $n2_text;
				
				/*$ds_text = get_sub_field('pravilnyj_otvet_opisanie');
				$descsuc[] = $ds_text;
				
				$de_text = get_sub_field('nepravilnyj_otvet_opisanie');
				$descerr[] = $de_text;*/
            }
        }
        
        $imgbg = [];
		
		if( have_rows('foto_igry_viktorina', 'options') ) {
            while( have_rows('foto_igry_viktorina', 'options') ) {
                the_row(); 
                
				$attachment = get_sub_field('foto');
                $image = wp_get_attachment_image_url( $attachment["ID"], "full" ); 
                
                $imgbg[] = $image;
            }
        }
        
        $arrvop = array('<a href="#" class="button-30 w-button vsuccess">'. $pravvop[0] .'</a>',
                       '<a href="#" class="button-30 w-button verror">'. $nepvop1[0] .'</a>', 
                       '<a href="#" class="button-30 w-button verror">'. $nepvop2[0] .'</a>');
                       
        shuffle( $arrvop );
        
        ?>
        
        <div class="div-block-819" style="background-image:url('<?= $imgbg[0]; ?>');">
            <div class="div-block-835">
              <div class="div-block-836">
                <div class="text-block-351"><?= $voprosi[0]; ?></div>
                <div class="div-block-837">
                </div>
              </div>
            </div>
            <div class="div-block-834">
                <?= $arrvop[0]; ?>
                <?= $arrvop[1]; ?>
                <?= $arrvop[2]; ?>
            </div>
        </div>
        
        <?php
		
		
		$myvariable = ob_get_clean();
		echo $myvariable;
        
        
        wp_die();
    }
    
    
    add_action('wp_ajax_viktorina_verror', 'viktorina_get_errveiw');
    add_action('wp_ajax_nopriv_viktorina_verror', 'viktorina_get_errveiw');
    
    function viktorina_get_errveiw(){
        
        ob_start();
		
        $descerr = [];
		
		if( have_rows('dlya_igry_viktorina', 'options') ) {
            while( have_rows('dlya_igry_viktorina', 'options') ) {
                the_row(); 
                
				$de_text = get_sub_field('nepravilnyj_otvet_opisanie');
				$descerr[] = $de_text;
            }
        }
        
        $imgbg = [];
		
		if( have_rows('foto_igry_viktorina', 'options') ) {
            while( have_rows('foto_igry_viktorina', 'options') ) {
                the_row(); 
                
				$attachment = get_sub_field('foto_proigrysh');
                $image = wp_get_attachment_image_url( $attachment["ID"], "full" ); 
                
                $imgbg[] = $image;
            }
        }
        
        ?>
        
        <div class="div-block-819" style="background-image:url('<?= $imgbg[ $_POST['id'] ]; ?>');">
            <div class="div-block-835-copy44">
              <div class="text-block-357"><?= $descerr[ $_POST['id'] ]; ?></div>
              <a href="#" class="button-32-copy w-button errnext">Другой вопрос</a>
            </div>
        </div>
        
        <?php
		
		
		$myvariable = ob_get_clean();
		echo $myvariable;
        
        
        wp_die();
    }
    
    
    add_action('wp_ajax_viktorina_vsuccess', 'viktorina_get_succveiw');
    add_action('wp_ajax_nopriv_viktorina_vsuccess', 'viktorina_get_succveiw');
    
    function viktorina_get_succveiw(){
        
        ob_start();
		
        $descsuc = [];
		
		if( have_rows('dlya_igry_viktorina', 'options') ) {
            while( have_rows('dlya_igry_viktorina', 'options') ) {
                the_row(); 
                
				$ds_text = get_sub_field('pravilnyj_otvet_opisanie');
				$descsuc[] = $ds_text;
            }
        }
        
        $imgbg = [];
		
		if( have_rows('foto_igry_viktorina', 'options') ) {
            while( have_rows('foto_igry_viktorina', 'options') ) {
                the_row(); 
                
				$attachment = get_sub_field('foto_pobeda');
                $image = wp_get_attachment_image_url( $attachment["ID"], "full" ); 
                
                $imgbg[] = $image;
            }
        }
        
        ?>
        
        <div class="div-block-819" style="background-image:url('<?= $imgbg[ $_POST['id'] ]; ?>');">
            <div class="div-block-835-copy44">
              <div class="text-block-357"><?= $descsuc[ $_POST['id'] ]; ?></div>
              <a href="#" class="button-32 w-button succnext">Другой вопрос</a>
            </div>
        </div>
        
        <?php
		
		
		$myvariable = ob_get_clean();
		echo $myvariable;
        
        
        wp_die();
    }
    
    
    
    add_action('wp_ajax_viktorina_next', 'viktorina_next_veiw');
    add_action('wp_ajax_nopriv_viktorina_next', 'viktorina_next_veiw');
    
    function viktorina_next_veiw(){
        
        ob_start();
		
        $voprosi = [];
        $pravvop = [];   
        $nepvop1 = [];
        $nepvop2 = [];
        //$descsuc = [];
        //$descerr = [];
		
		if( have_rows('dlya_igry_viktorina', 'options') ) {
            while( have_rows('dlya_igry_viktorina', 'options') ) {
                the_row(); 
                
				$v_text = get_sub_field('vopros');
				$voprosi[] = $v_text;
				
				$p_text = get_sub_field('pravilnyj_otvet');
				$pravvop[] = $p_text;
				
				$n1_text = get_sub_field('1_nepravilnyj_otvet');
				$nepvop1[] = $n1_text;
				
				$n2_text = get_sub_field('2_nepravilnyj_otvet');
				$nepvop2[] = $n2_text;
				
				/*$ds_text = get_sub_field('pravilnyj_otvet_opisanie');
				$descsuc[] = $ds_text;
				
				$de_text = get_sub_field('nepravilnyj_otvet_opisanie');
				$descerr[] = $de_text;*/
            }
        }
        
        $imgbg = [];
		
		if( have_rows('foto_igry_viktorina', 'options') ) {
            while( have_rows('foto_igry_viktorina', 'options') ) {
                the_row(); 
                
				$attachment = get_sub_field('foto');
                $image = wp_get_attachment_image_url( $attachment["ID"], "full" ); 
                
                $imgbg[] = $image;
            }
        }
        
        if (isset( $pravvop[ $_POST['vprid'] ] )) {
            $arrvop = array('<a href="#" class="button-30 w-button vsuccess">'. $pravvop[ $_POST['vprid'] ] .'</a>',
                       '<a href="#" class="button-30 w-button verror">'. $nepvop1[ $_POST['vprid'] ] .'</a>', 
                       '<a href="#" class="button-30 w-button verror">'. $nepvop2[ $_POST['vprid'] ] .'</a>');
                       
            $vpr = $voprosi[ $_POST['vprid'] ];
        } else {
            $arrvop = array('<a href="#" class="button-30 w-button vsuccess">'. end( $pravvop ) .'</a>',
                       '<a href="#" class="button-30 w-button verror">'. end( $nepvop1 ) .'</a>', 
                       '<a href="#" class="button-30 w-button verror">'. end( $nepvop2 ) .'</a>');
                       
            $vpr = end( $voprosi );
        }
                       
        shuffle( $arrvop );
        
        if (isset( $imgbg[ $_POST['imgid'] ] )) {
            $fon = $imgbg[ $_POST['imgid'] ];
        } else {
            $fon = end( $imgbg );
        }
        
        ?>
        
        <div class="div-block-819" style="background-image:url('<?= $fon; ?>');">
            <div class="div-block-835">
              <div class="div-block-836">
                <div class="text-block-351"><?= $vpr; ?></div>
                <div class="div-block-837">
                </div>
              </div>
            </div>
            <div class="div-block-834">
                <?= $arrvop[0]; ?>
                <?= $arrvop[1]; ?>
                <?= $arrvop[2]; ?>
            </div>
        </div>
        
        <?php
		
		
		$myvariable = ob_get_clean();
		echo $myvariable;
        
        
        wp_die();
    }
    
    