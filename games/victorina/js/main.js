$(document).ready(function() 
{
    var otv = false;
    var vpr = 0;
    var img = 0;
    
    $(document).on('click', '.victorina-start', function(){
        
        if ( otv )
            return;
            
        otv = true;
        
        $.post( myajax.url, {'action': 'viktorina_init'}, function(response) {
            otv = false;
            $('._2game').html( response );
		});
		
    });
    
    $(document).on('click', '.vsuccess', function(){
        
        if ( otv )
            return;
            
        otv = true;
        
        $(this).css({
            'background-color' : 'rgb(60, 151, 64)',
            'transform' : 'translate3d(0px, 0px, 0px) scale3d(1.1, 1.1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg)',
            'transform-style' : 'preserve-3d'
        });
        
        setTimeout(function(){
            $.post( myajax.url, {'action': 'viktorina_vsuccess', 'id' : img}, function(response) {
                otv = false;
                $('._2game').html( response );
    		});
        }, 500);
    });
    
    $(document).on('click', '.verror', function(){
        
        if ( otv )
            return;
            
        otv = true;
        
        $(this).css({
            'background-color' : 'rgb(219, 57, 45)',
            'transform' : 'translate3d(0px, 0px, 0px) scale3d(1, 1, 1) rotateX(0deg) rotateY(0deg) rotateZ(0deg) skew(0deg, 0deg)',
            'transform-style' : 'preserve-3d'
        });
        
        setTimeout(function(){
            $.post( myajax.url, {'action': 'viktorina_verror', 'id' : img}, function(response) {
                otv = false;
                $('._2game').html( response );
    		});
        }, 500);
		
    });
    
    $(document).on('click', '.errnext', function(){
        
        if ( otv )
            return;
            
        otv = true;
        
        vpr++;
        
        setTimeout(function(){
            $.post( myajax.url, {'action': 'viktorina_next', 'vprid' : vpr, 'imgid' : img}, function(response) {
                otv = false;
                $('._2game').html( response );
    		});
        }, 500);
		
    });
    
    //succnext
    $(document).on('click', '.succnext', function(){
        
        if ( otv )
            return;
            
        otv = true;
        
        vpr++;
        img++;
        
        setTimeout(function(){
            $.post( myajax.url, {'action': 'viktorina_next', 'vprid' : vpr, 'imgid' : img}, function(response) {
                otv = false;
                $('._2game').html( response );
    		});
        }, 500);
		
    });
    
});