<h1>Страница настроек расписания сотрудников</h1>

<form method="POST">
	<input type="hidden" name="calendar_spa" value="calendar">

	<h3>График работ</h3>
	
	<?php if ($saved) { ?>
		<div class="alert alert-info alert-dismissible">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Сохранено!</strong>
		</div>
	<?php } ?>
	
	<table class="tg">
		<tr>
			<th class="tg-obcv" rowspan="3">Мастера/День</th>
			<th class="tg-qtf5" colspan="2"><?php echo date('d.m', strtotime("+0 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-73oq" colspan="2"><?php echo date('d.m', strtotime("+1 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-73oq" colspan="2"><?php echo date('d.m', strtotime("+2 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-73oq" colspan="2"><?php echo date('d.m', strtotime("+3 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-73oq" colspan="2"><?php echo date('d.m', strtotime("+4 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-73oq" colspan="2"><?php echo date('d.m', strtotime("+5 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-73oq" colspan="2"><?php echo date('d.m', strtotime("+6 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+7 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+8 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+9 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+10 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+11 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+12 days -".$spa_smolnii_mon." hours")); ?></th>
			<th class="tg-wp8o" colspan="2"><?php echo date('d.m', strtotime("+13 days -".$spa_smolnii_mon." hours")); ?></th>
		</tr>
		<tr>
			<td class="tg-obcv">д</td>
			<td class="tg-obcv">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-wp8o">н</td>
			<td class="tg-wp8o">д</td>
			<td class="tg-73oq">н</td>
		</tr>
		<tr>
			<td class="tg-obcv" colspan="2"><?php echo getDayRus("0"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("1"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("2"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("3"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("4"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("5"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("6"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("7"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("8"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("9"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("10"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("11"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("12"); ?></td>
			<td class="tg-wp8o" colspan="2"><?php echo getDayRus("13"); ?></td>
		</tr>
		
		<?php
			$args = array(
				'post_type' => 'master',
				'post_status'    => 'publish',
				'orderby'        => 'name',
				'order'          => 'ASC',
				'posts_per_page' => '500',
				'tax_query' => array( 
					array(
						'taxonomy' => 'master-cat',
						'field'    => 'slug',
						'terms'    => array('smolnyj')
					)
				)
			);
			$query = new WP_Query( $args );
			usort($query->posts, "comp");
			
			if ( $query->have_posts() ) : 
				while ( $query->have_posts() ) : 
					$query->the_post();	
		?>
			
			<tr>
				<td class="tg-cum3"><?php the_title(); ?></td>
				
				<?php 
					for ($i = 0; $i<28; $i++) { 

						$checked = false;
						$index_date = date('d.m', strtotime("+". intval($i / 2) ." days -".$spa_smolnii_mon." hours"));
			
						if ($i % 2 == 0) {
							if (isset ($cal_smolnii_d[$index_date][get_the_ID()]))
								$checked = $cal_smolnii_d[$index_date][get_the_ID()] == 'on' ? true : false;
						} else {
							if (isset ($cal_smolnii_n[$index_date][get_the_ID()]))
								$checked = $cal_smolnii_n[$index_date][get_the_ID()] == 'on' ? true : false;
						}
				?>
				
					<td class="<?= ($i%2) ? "tg-j3py" : "tg-73oq";?> tg-pd0">
						<label class="container">
							<input name="cal_smolnii_<?= ($i % 2 == 0) ? 'd' : 'n'; ?>[<?=$index_date;?>][<?=get_the_ID();?>]" type="checkbox" <?= ($checked == true) ? 'checked' : '' ?>>
							<span class="checkmark"></span>
						</label>
					</td>
					
			<?php } ?>
			</tr>
		
		<?php
				endwhile;  
				wp_reset_query(); 
			endif;   
		?>
		
	</table>
	<br>
	
	<h3>Настройки времени расписания и часового пояса</h3>

	<table class="form-table">
    	<tbody>
			<tr>
				<th><label for="spa_zone">Текущее системное время</label></th>
				<td id="dataCircle"><?=date("Y-m-d H:i:s");?></td>
			</tr>


    		<tr>
    			<th><label for="spa_smolnii_mon">Начало дневной смены</label></th>
    			<td> 
    			    <input name="spa_smolnii_mon" id="spa_smolnii_mon" type="text" value="<?= $spa_smolnii_mon; ?>" class="regular-text code">
    			</td>
    		</tr>
    		
    		<tr>
    			<th><label for="spa_smolnii_night">Начало ночной смены</label></th>
    			<td> 
    			    <input name="spa_smolnii_night" id="spa_smolnii_night" type="text" value="<?= $spa_smolnii_night; ?>" class="regular-text code">
    			</td>
    		</tr>
    	</tbody>
    </table>
	
    <input type="submit" value="Сохранить" class="button button-primary button-large">
</form>