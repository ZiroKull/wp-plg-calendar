<h1>Страница настроек для автопостинга в VK</h1>

<form method="POST">
	<input type="hidden" name="vk_settings" value="vk_settings">

	<h3>Настройки</h3>
	
	<?php if ($saved) { ?>
		<div class="alert alert-info alert-dismissible">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Сохранено!</strong>
		</div>
	<?php } ?>
	
	<table class="form-table">
    	<tbody>
    	    <tr>
    			<th><label for="vk_token">VK token</label></th>
    			<td> <input name="vk_token" id="vk_token" type="text" value="<?= $vk_token; ?>" class="regular-text code"></td>
    		</tr>
			<tr>
    			<th><label for="vk_group">VK group id</label></th>
    			<td> <input name="vk_group" id="vk_group" type="text" value="<?= $vk_group; ?>" class="regular-text code"></td>
    		</tr>
    		<tr>
    			<th><label for="vk_desc_d">Описание дневной смены</label></th>
    			<td>
    			    <p>Для вставки кол-ва мастеров использйте {spa} в тексте.</p>
    			    <textarea name="vk_desc_d" id="vk_desc_d" rows="5" class="regular-text code"><?= $vk_desc_d; ?></textarea>
    			</td>
    		</tr>
    		<tr>
    			<th><label for="vk_desc_n">Описание ночной смены</label></th>
    			<td>
    			    <p>Для вставки кол-ва мастеров использйте {spa} в тексте.</p>
    			    <textarea name="vk_desc_n" id="vk_desc_n" rows="5" class="regular-text code"><?= $vk_desc_n; ?></textarea>
    			</td>
    		</tr>
    		<tr>
    			<th><label for="vk_smile">Смайлик перед именем мастера</label></th>
    			<td> <input name="vk_smile" id="vk_smile" type="text" value="<?= $vk_smile; ?>" class="regular-text code"></td>
    		</tr>
    		<tr>
    			<th><label for="vk_cont_desc">Краткое описание контактов</label></th>
    			<td> <input name="vk_cont_desc" id="vk_cont_desc" type="text" value="<?= $vk_cont_desc; ?>" class="regular-text code"></td>
    		</tr>
    		<tr>
    			<th><label for="vk_phone">Телефон</label></th>
    			<td> <input name="vk_phone" id="vk_phone" type="text" value="<?= $vk_phone; ?>" class="regular-text code"></td>
    		</tr>
    		<tr>
    			<th><label for="vk_adress">Адресс</label></th>
    			<td> <input name="vk_adress" id="vk_adress" type="text" value="<?= $vk_adress; ?>" class="regular-text code"></td>
    		</tr>
    		<tr>
    			<th><label for="vk_link">Ссылка на сайта</label></th>
    			<td> <input name="vk_link" id="vk_link" type="text" value="<?= $vk_link; ?>" class="regular-text code"></td>
    		</tr>
    		<tr>
    			<th><label for="vk_tags">Тэги</label></th>
    			<td>
    			    <p>Тэги выделяйте через зяпятую пример "PremierSpa, девушкипермь" и т. п.</p> 
    			    <textarea name="vk_tags" id="vk_tags" rows="5" class="regular-text code"><?= $vk_tags; ?></textarea>
    			</td>
    		</tr>
    	</tbody>
    </table>
    
    <h3>Настройки времени публикации в VK и часового пояса</h3>
	
	<table class="form-table">
    	<tbody>
    	    <tr>
    			<th><label for="vk_zone">Часовой пояс</label></th>
    			<td> 
                    <select class="regular-text code" name="vk_zone" id="vk_zone">
                        
                        <?php
                            $zones = ['-12', '-11', '-10', '-9', '-8', '-7', '-6', '-5', '-4', '-3', '-2', '-1', '0', '+1', '+2', '+3', '+4', '+5', '+6', '+7', '+8', '+9', '+10', '+11', '+12'];
                            
                            foreach ($zones as $zone => $value) {
                                $selected = ($vk_zone ===  $value) ? 'selected="selected"' : '';
                                
                                echo '<option '. $selected .' value="' . $value . '">GMT ' . $value . '</option>';
                            }
                        ?>
                    </select>
    			</td>
    		</tr>
    		
    		<tr>
    			<th><label for="vk_mon">Время публикации днем</label></th>
    			<td> 
    			    <input name="vk_mon" id="vk_mon" type="text" value="<?= $vk_mon; ?>" class="regular-text code">
    			</td>
    		</tr>
    		
    		<tr>
    			<th><label for="vk_night">Время публикации ночью</label></th>
    			<td> 
    			    <input name="vk_night" id="vk_night" type="text" value="<?= $vk_night; ?>" class="regular-text code">
    			</td>
    		</tr>
    	</tbody>
    </table>
    
    <br>
	
    <input type="submit" value="Сохранить" class="button button-primary button-large">
</form>

<form method="POST">
	<input type="hidden" name="vk_publish" value="vk_publish">

	<h3>Опубликовать пост</h3>
	
    <input type="submit" value="Опубликовать" class="button button-primary button-large">
</form>

